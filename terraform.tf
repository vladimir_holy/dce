
terraform {
  required_providers {
    opennebula = {
      source = "OpenNebula/opennebula"
      version = "~> 1.2"
    }
  }
}

provider "opennebula" {
  endpoint      = "https://nuada.zcu.cz/RPC2"
  username      = "${var.o_username}"
  password      = "${var.o_password}"
}

resource "opennebula_image" "os-image" {
    name = "Ubuntu"
    datastore_id = 101
    persistent = false
    path = "https://marketplace.opennebula.io//appliance/4562be1a-4c11-4e9e-b60a-85a045f1de05/download/0"
    permissions = "600"
}

resource "opennebula_virtual_machine" "frontend_node" {
  name = "frontend_node"
  description = "Frontend node"
  cpu = 1
  vcpu = 1
  memory = 2048
  permissions = "600"
  group = "users"

  context = {
    NETWORK  = "YES"
    HOSTNAME = "$NAME"
    SSH_PUBLIC_KEY = "${var.ssh_public}"
  }

  os {
    arch = "x86_64"
    boot = "disk0"
  }

  disk {
    image_id = opennebula_image.os-image.id
    target   = "vda"
    size     = 12000
  }

  graphics {
    listen = "0.0.0.0"
    type   = "vnc"
  }

  nic {
    network_id = 3
  }

  connection {
    type = "ssh"
    user = "root"
    host = "${self.ip}"
    private_key = "${file("/root/.ssh/id_ecdsa")}"
  }

  tags = {
    role = "frontend"
  }
}

resource "opennebula_virtual_machine" "backend_node" {
  count = var.backend_count
  name = "backend_node"
  description = "Backend node - ${count.index + 1}"
  cpu = 1
  vcpu = 1
  memory = 2048
  permissions = "600"
  group = "users"

  context = {
    NETWORK  = "YES"
    HOSTNAME = "$NAME"
    SSH_PUBLIC_KEY = "${var.ssh_public}"
    START_SCRIPT = "echo '${count.index + 1}' >> /tmp/index"
  }

  os {
    arch = "x86_64"
    boot = "disk0"
  }

  disk {
    image_id = opennebula_image.os-image.id
    target   = "vda"
    size     = 12000
  }

  graphics {
    listen = "0.0.0.0"
    type   = "vnc"
  }

  nic {
    network_id = 3
  }

  connection {
    type = "ssh"
    user = "root"
    host = "${self.ip}"
    private_key = "${file("/root/.ssh/id_ecdsa")}"
  }

  tags = {
    role = "backend"
  }
}

#OUTPUTS

output "frontend_node" {
  value = "${opennebula_virtual_machine.frontend_node.*.ip}"
}

output "backend_node" {
  value = "${opennebula_virtual_machine.backend_node.*.ip}"
}

resource "local_file" "cluster_cfg" {
  content = templatefile("templates/inventory.tmpl",
    {
      frontend_node = opennebula_virtual_machine.frontend_node.*.ip,
      backend_node = opennebula_virtual_machine.backend_node.*.ip
    })
  filename = "./dynamic_inventories/cluster"
}

resource "local_file" "host_cfg" {
  content = templatefile("templates/hosts.tmpl",
    {
      frontend_node = opennebula_virtual_machine.frontend_node.*.ip,
      backend_node = opennebula_virtual_machine.backend_node.*.ip
    })
  filename = "./ansible/frontend/hosts"
}

resource "local_file" "default_cfg" {
  content = templatefile("templates/000-default.tmpl",
    {
      backend_node = opennebula_virtual_machine.backend_node.*.ip
    })
  filename = "./ansible/frontend/000-default.conf"
}

resource "local_file" "web_cfg" {
  content = templatefile("templates/web.tmpl",
    {
      frontend_node = opennebula_virtual_machine.frontend_node.*.ip
    })
  filename = "./web_address.txt"
}



