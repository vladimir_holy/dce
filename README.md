# Load Balancer

## Popis aplikace
Aplikace představuje webovou stránku s implementovaným load balancerem. K implememtaci byly použity nástroje Docker, Terraform, Ansible a Apache HTTP Server. Jako cludová služba je využita OpenNebula. Vzhled webový stránky je velice jednoduchý a to pouze s nápisem 'Backend <číslo>', kde <číslo> představuje index backendového serveru. 
## Struktura
V hlavním adresáři aplikace se nachází celkem čtyři soubory (terraform.tf, terraform.tfstate, terraform.tfvars, variables.tf) a čtyři další adresáře (.devcontainer, .terraform, ansible, templates). V dalších částech popíšu důležíté soubory a adresáře.

#### terraform.tf
V tomto souboru je popsaná struktura frontend a backend nodů, parametry obrazu operačního systému a připojení na cloudovou službu.

#### variables.tf
V tomto souboru jsou uložené čtyři důležíté proměnné aplikace. První z nich je proměnná 'o_username', kde je nutné vyplnit jméno účtu na cloudové službě Open Nebula. Další proměnnou je 'o_password', kde je nutné zadat z cloudové služby vygenerovaný autorizační token. Třetí proměnnou je 'backend_count', která slouží ke konfiguraci počtu backendových serverů. Poslední proměnou je 'ssh_public', kam je nutné zadat veřejný ssh klíč pro následné připojení k serverům. 

### Adresář templates
V tomto adresáři se nachází celkem čtyři soubory, které slouží jako předloha pro generování souborů při vytváření serverů.

### Adresář ansible
V tomto adresáři se nacházejí soubory, které konfigurují práci nástroje Ansible. Pomocí těchto souborů je nakonfigurováno stažení potřebných nástrojů a závislostí na již běžících nodech, nakopírování souborů, spuštění webových serverů a nakonfigurování load balanceru.

## Sestavení aplikace
Pro sestavení aplikace je nutné adresář s aplikací otevřít v Docker kontejneru. Dále je duležité mít nainstalovaný nástroj Terraform a Ansible a vygenerovaný token z cloudové služby Open Nebula. Dále je potřebné mít vygenerovaný veřejný ssh klíč. Do souboru variable.tf s proměnnými aplikace vyplníme potřebné informace a zvolíme počet backendových serverů. V kmenovém adresáři aplikace si otevřeme terminál a zavoláme tyto dva příkazy:
```sh
terraform init
terraform plan
```
Pomocí těchto příkazů se inicializuje a naplánuje nástroj Terraform na náš projekt. Ten následně spustíme příkazem:
```sh
terraform apply
```
Po skončení tohoto příkazu se nám již na Open Nebule nacházejí běžící frontend a backend nody. Pro nahrání potřebných souborů, stažení důležitých nástrojů a spuštění samotných webových serverů je důležité použít ještě nástroj Ansible. Ten se použije zavoláním tohoto příkazu:
```sh
ansible-playbook -i dynamic_inventories/cluster ansible/cluster.yml
```
Po doběhnutí tohoto příkazu již běží na frontendovém nodů webový server s load balancerem. Dále se také vygeneruje v hlavním adresáři soubor 'web_address.txt' s webovou adresou pro připojení na server. Tu lze jednoduše vložit do jakéhokoliv prohlížeče.
