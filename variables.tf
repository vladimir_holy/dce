#Opennebula username
variable "o_username"  {
    description = "Open Nebula username"
    default = "username"
}

#Opennebula token
variable "o_password"  {
    description = "Open Nebula password"
    default = "token"
}

#Number of backends nodes
variable "backend_count"  {
    description = "Number of backends nodes"
    default = 2
}

#SSH public key
variable "ssh_public"  {
    description = "SSH public key"
    default = "ssh_public_key"
}

